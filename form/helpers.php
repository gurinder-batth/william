<?php

function error_msg($field){
    if(isset($_SESSION[$field])) { ?>
         <span class="help has-text-danger"> <?= $_SESSION[$field] ?>  </span>
          <?php  unset($_SESSION[$field]);
    }
}

function validateLength($err,$field,$min,$max,$message = null){
      if(strlen($field) < $min || strlen($field) > $max){
         
         if($message == null){
            $message = "$err shoud be more than  $min chars and less than $max chars";
          }

          $_SESSION['errors_'.$err]  = $message;
           header("Location:".$_SERVER['HTTP_REFERER']);
     }
}


// Validation Start Here Bro
function validate($rules){
    foreach($rules as $key => $rule){
        $methods = explode('|',$rule);
        foreach($methods as $method){
            $fun = explode(':',$method);
            // that's mean method has argument
              if(count($fun) == 2){
                       $fun[0] =  'v_'.$fun[0];
                       $fun[0]($key,$fun[1]);
              } else{
                   //   Method has no argument
                       $fun[0] =  'v_'.$fun[0];
                       $fun[0]($key);
              }
        }
    }
}

function v_required($key){
    if($_POST[$key] == null){
        redirect($key,$key . " is required");
    }
}

function v_min($key,$len){

}

function v_max($key,$len){

}

function v_numeric($key){

}

function v_char($key){

}

function redirect($key,$message){
    $_SESSION['errors_'.$key]  = $message;
     header("Location:".$_SERVER['HTTP_REFERER']);
}