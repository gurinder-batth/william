<?php session_start();  include_once 'helpers.php' ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <title>Form</title>
</head>

<body>

<!-- GET: data shown in url -->
<!-- Tip: Any website we open or a tag in browser all belongs to get method -->
<!-- Form either Get or Post -->
<!-- Google.com -->
<!-- Server -->
<!-- Server check method Type -->


    <div class="container">
        <form action="login.php" method="POST">
            <div class="section">
                <div class="field">
                    <label for="">Username</label>
                    <input type="text" class="input" name="username">
                     <?= error_msg("errors_username") ?>
                </div>
                <div class="field">
                    <label for="">Password</label>
                    <?= error_msg("errors_password") ?>
                    <input type="text" class="input" name="password">
                    
                </div>
                <div class="field">
                    <button class="button is-dark">Submit</button>
                </div>
            </div>
        </form>
    </div>

</body>

</html>