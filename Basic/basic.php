<?php
//Loosely Varibale Type Programming Language (PHP)

//Eg: C , Java , C++ , Python
// int i; String 

// i = 1; //var_dump()
//i = "2" // String ''  ""

// $i = 1;

// echo $i;

//$students;

// print_r , print , var_dump() //Debugging Methods

//1D array

// $students = [
// "Gurinder" , // 0   
// "William" ,
// "Sanjeev" ,
// "Pankaj" 
// ]; //Index Array (integer)


// //Index Array & Associtive Array

// for($i = 0; $i < count($students) ; $i++){
//     echo $students[$i]  .  "<br>" ;
// }

//Second Array

// $students = [

//     [
//         "Gurinder" , //00
//         "Sangrur" 
//     ], //0

//     [
//         "William" , //10
//         "Ludhiana"
//     ] //1
// ]; //Index Array (integer)

//String 
// echo "<pre>";
// print_r($students);

// echo "<pre>";
// for ($i=0; $i < count($students) ; $i++) { 
//      for ($j=0; $j <  count($students[$i]); $j++) { 
//           print_r($students[$i][$j] . "<Br>");
//      }
// }

// $students = [

//         [
//            "name" =>  "Gurinder" , //
//            "city" =>  "Sangrur" 
//         ], //0
    
//         [
//             "name" => "William" , //10
//             "city" => "Ludhiana"
//         ] //1
//     ]; //Index Array (integer)

    // echo $students[0]["name"];
    // echo $students[0]["city"];

    // for ($i=0; $i < count($students) ; $i++) { 
    //       echo $students[$i]['name'] . "  " . $students[$i]['city'];
    // }

    // foreach($students as $student){
    //     echo $student['name'] . "  " . $student['city'] . "<br>";
    // }