<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HTML RENDERING</title>
</head>
<body>
    
    <?php   
       $students = [
                    [
                    "name" =>  "Gurinder" , //
                    "city" =>  "Sangrur" 
                    ], //0
                
                    [
                        "name" => "William" , //10
                        "city" => "Ludhiana"
                    ] //1
            ];
    ?>


        <table border="1" width="500">
            <tr>
                   <td>Sr No.</td>
                    <td width="250">Name</td>
                    <td width="250">City</td>
                </tr>
            <?php
                foreach($students as $key => $student){ ?>            
                <tr>
                    <td> <?= $key+1 ?> </td>
                    <td><?=$student['name']?></td>
                    <td><?=$student['city'] ?></td>
                </tr>
                <?php } ?>
            </table>
                <?php         
    ?>

</body>
</html>